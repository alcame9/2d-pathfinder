/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/


#ifndef __FINDER__
#define __FINDER__
#include "glm\glm.hpp"
#include "glm/gtx/matrix_transform_2d.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <vector>

class Map;
class Node;
class SearchEngine{
 
  Node * start_node_;
  Node * target_node_;
  Map * map_;
  std::vector<Node> found_way_;
  //Lists
  std::vector<Node> open_list_;
  std::vector<Node> closed_list_;
  //--------------FUNCTIONS------------//
  void addNodeOpenList(Node node);
  Node* findNodeOpenList(Node node);
  Node* findNodeCloseList(Node node);
public:
  SearchEngine();
  ~SearchEngine();
  void init();
  std::vector<Node> findWay(Map* map,Node* start_node, Node* target_node,bool show_time);


};
#endif