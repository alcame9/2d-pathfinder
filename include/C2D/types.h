/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/
#ifndef _TYPES_

#define _TYPES_
#include <stdint.h>
#include <iostream>
#include <string>
#include <vector>
#include "math_L.h"
typedef  int32_t  int32;
typedef  int16_t  int16;
typedef  int8_t   int8;
typedef std::string string;



#endif // !TYPES_H_
