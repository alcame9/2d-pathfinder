
/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
* @brief Main header of the library
*/

#ifndef _DRAW_
#define _DRAW_
#include "math_L.h"
namespace C2D {
	/*
	* @brief   Call this function before call any draw funtion
				to prepare the render pass

	*/
	void BeginDraw();

	/*
	* @brief   Call this function after all draw functions but
	before C2D::RenderFrame(); .
	*/
	void EndDraw();

	/*
	* @brief Draws a shape filled or not.
	* @param number_points   Number of points in the shape
	* @param points   Array with the points
	* @param filled   Shoud the shape be filled?
	*  
	* float points [] {
			point1_x, point1_y,
			point2_x_, point2_y,
			point3_x_,point3_y,

	}; 
	void DrawSolidShap(3,points,true);

	*/
	void DrawSolidShape(int number_points, float points[20],bool filled);

	/*
	* @brief   Draws a line in screen pixels
	* @param x1   Coordinate X for the firts point
	* @param y1   Coordinate Y for the firts point
	* @param x2   Coordinate X for the second point
	* @param y2   Coordinate Y for the second point
	*/
	void DrawLine(int x1, int y1, int x2, int y2);

	/*
	* @brief   Set the color for draw lines in RGB ( 0 - 255 ) format.
	* @param r   Red value
	* @param g   Green value
	* @param b   Blue value

	*/
	void SetColorStroke(float r, float g, float b);

	/*
	* @brief   Set the color for solid shapes in RGB ( 0 - 255 ) format.
	* @param r   Red value
	* @param g   Green value
	* @param b   Blue value

	*/
	void SetColorFillShape(float r, float g, float b);
}

#endif // !_DRAW_
