/*Copyright 2018 . Alejandro Canela Mendez
 * @ author Alejandro Canela Mendez
 * @brief Main header of the library
*/
#ifndef _C2D_
#define _C2D_
#include "types.h"

namespace C2D {

	/*
	* @brief   Call this function to initialize the library
	* @param width   Window width
	* @param height   Window height
	* @param window_title   Window title
	* @return true if everything is correct or false if somethig goes wrong
	during the initialization
	*/
	bool Init(int16 width, int16 height,const char* window_title);

	/*
	* @brief  This function calls all render commands. 
	* Must be called after all draw functions

	*/
	void RenderFrame();

	/*
	* @brief  This function returns if the window should close
	* @return True if the window is going to close
	*/
	bool ShouldClose();
	/*
	@brief   Call this function to destroy all graphic resources.
			Shoud be called at the end of the rogram
	*/
	void Destroy();



}

#endif // !C2D