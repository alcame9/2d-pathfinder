/*Copyright 2018 . Alejandro Canela Mendez
*
* Time header to acces the times in seconds and ms
*/

#ifndef _TIME_H_
#define _TIME_H_
	double GetSecondsTime();
	double GetMiliTime();
#endif



