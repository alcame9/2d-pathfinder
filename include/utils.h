
#ifndef __U_UTILS__
#define __U_UTILS__
#include "glm\glm.hpp"
#define   SOLDIER_PATH "agents/axis_soldier.bmp"
#define   PRISIONER_PATH "agents/allied_soldier.bmp"
#define   GUARD_PATH "agents/allied_dead_agent.bmp"

#define   OPEN_V_DOOR_PATH "final/door_v_open.bmp"
#define   CLOSED_V_DOOR_PATH "final/door_v_closed.bmp"
#define   OPEN_H_DOOR_PATH "final/door_h_open.bmp"
#define   CLOSED_H_DOOR_PATH "final/door_h_closed.bmp"
//"agents/axis_soldier.bmp"
//Vaariables

const int WORKING_ZONE = 0;
const int REST_ZONE = 1;
const int YARD_ZONE = 2;
const int VALLE_ZONE = 3;
const int HALL_ZONE = 4;

const int HALL_COLOR = 582;
const int WORK_COLOR = 684;
const int YARD_COLOR = 634;
const int VALLE_COLOR = 643;
const int REST_COLOR = 287;
const glm::vec2 h_door_positions_[3] = { glm::vec2(18,27),
                                      glm::vec2(19,27),
                                       glm::vec2(20,27) };

const glm::vec2 v_door_positions_[3] = { glm::vec2(102, 32),
                                         glm::vec2(102, 33),
                                         glm::vec2(102, 34) };

const glm::vec2 load_box_p = glm::vec2(400.0, 275.0);
const glm::vec2 unload_box_p = glm::vec2(670.0, 275.0);
#endif
