/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/

#ifndef __MAP__
#define __MAP__
#include <vector>
#include "node.h"
#include "C2D\sprite.h"
#include "C2D\draw.h"
#include "glm\glm.hpp"
#include "glm/gtx/matrix_transform_2d.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <map>
class Map{
  static const int number_of_paths = 20;



  std::vector<Node> map_mat_;
  std::vector<Node*> working_zone_;
  std::vector<Node*> rest_zone_;
  std::vector<Node*> yard_zone_;

  std::vector<Node> base_a_;
  std::vector<Node> base_b_;
  std::vector<Node> finded_way;

  C2D::Sprite map_sprite_;

  float not_walkable_color[4];
  int collision_map_height_, collision_map_width_;
  int grid_mouse_position_x, grid_mouse_position_y;
  //Points to start and target point
  int start_x, start_y, target_x, target_y;

  void readMapSprite(const char *file_name);
  void renderQuad(int x, int y, int widht, int heigh, float color_data[4]);
  void renderGrid();


public:
  Map();
  std::vector<Node> getMap();
  void init(const char *map_sprite, const char *map_collision_sprite,const char* zones_map);
  void readCollisionMapFromImage(const char* file_name);
  void readZonesMap(const char *file_name);

  Node* getRandomNodeFromZone(int zone);
  Node* getStart();
  Node* getTarget();
  Node * getNodeByScreenPosition(int x, int y);

  int getHeight();
  int getWidth();
  int getHeightMapCollision();
  int getWidthMapCollision();

  void inputMap();
  void setFindedWay(std::vector<Node> way);
  void render();
  void renderWay(std::vector<Node> way);


  friend class Manager;
  friend class Node;


  int map_sprite_w_;
  int map_sprite_h_;
  int offset_maps_x;
  int offset_maps_y;

};

#endif