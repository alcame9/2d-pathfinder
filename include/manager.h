/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/

#ifndef __MANAGER__
#define __MANAGER__


#include "map.h"
#include "path_finding_engine.h"
class Manager{

  static Manager * manager_ptr;
  Manager();
	SearchEngine * finder_;
	Map* map_;
public:
  void init();
  static Manager& instance();
  Map* map();
  void update(float dt);
  void draw();
  SearchEngine* GetPathfinder();
  friend class Brain;
};
#endif // !__MANAGER__