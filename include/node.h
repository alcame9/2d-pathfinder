/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/

#include <stdio.h>
class Node{
  int x_;
  int y_;
  int g_;
  int h_;
  int f_;
  float pixels_x;
  float pixels_y;
  Node* parent_node_;

  float walkable_;
  bool is_door_;
  //-------------------------------//
  //FUNCTIONS//
  int computeH();
 
public:
  Node();
  Node(const Node& node);
  int * getPosition();
  bool operator==(const Node& rh);
  bool operator!=(const Node& rh);
  
  void init(int x, int y, 
          Node * parent_node, Node *final_node
          ,int total_g , bool walkable);
  void setParent(Node *parent);
  void setWalkable(bool walkable);
  void setCosts( int cost_G);

  int getG();
  int getF();
  bool isWalkable();
  Node * getParent();

  Node * final_node_;
  bool closed_;
  bool open_;
  int zone_type_;
  friend class Map;
};