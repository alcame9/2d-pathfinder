#include "manager.h"

#include "utils.h"
Manager* Manager::manager_ptr = nullptr;

Manager::Manager(){

}



void Manager::init(){
  finder_ = new SearchEngine();
  finder_->init();
  map_ = new Map();
  map_->init("final/map_03_960x704_layout.bmp", "final/map_03_120x88_bw.bmp", "final/map_03_zones01.bmp");

}

Manager& Manager::instance(){
  if (manager_ptr == nullptr){
    manager_ptr = new Manager();
  }
  return *manager_ptr;
}

void Manager::update(float dt){
	map_->inputMap();
}

void Manager::draw(){
	map_->render();
}

SearchEngine * Manager::GetPathfinder(){
	
	return &*finder_;
}

Map * Manager::map(){
  return map_;
}
