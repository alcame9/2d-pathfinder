/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
*/


#include "C2D\c2d.h"
#include "C2D\time.h"
#include "C2D\draw.h"
#include "C2D/sprite.h"
#include <stdio.h>
#include <random>
#include <time.h>
#include <iostream>
#include <memory>


const int W = 960;
const int H = 704;

#include "manager.h"
int main(int argc, char **argv){
  //Start manager
 
  std::srand(time(0));
//  initOpenAL();
  C2D::Init(W, H,"Pathfinde");
  srand(time(NULL));
  bool g_quit = false;
  double currentT = GetSecondsTime();
  double accum = 0.0;
  double dt = 0.01;
  double t = 0.0;

  Manager::instance().init();

  while (!C2D::ShouldClose()){
    double new_time = GetSecondsTime();
	double frame_time = new_time - currentT;
	currentT = new_time;
	accum += frame_time;

    while (accum >= dt){
      //for (int i = 0; i < 4; i++){
      Manager::instance().update(t);
	  accum -= dt;
	  t += dt;
     
    }


    C2D::BeginDraw();

    Manager::instance().draw();
    C2D::EndDraw();
    C2D::RenderFrame();
      
  }
  C2D::Destroy();
  return 0;
}
