#include "path_finding_engine.h"
#include "map.h"
#include "C2D\time.h"
#include <iostream>
#define NORMAL_FhysicalBody 10
#define DIAGONAL_FhysicalBody 14


SearchEngine::SearchEngine(){}
SearchEngine::~SearchEngine(){}

void SearchEngine::init(){

  start_node_ = nullptr;

}

std::vector<Node> SearchEngine::findWay(Map*map,Node * start_node, Node * target_node,bool show_time){
  
  if (start_node == nullptr || target_node == nullptr)return std::vector<Node>(0);
  start_node_ = start_node; target_node_ = target_node;
  if (!start_node_->isWalkable() || !target_node->isWalkable()){
    printf("Start node or target node are not walkable\n");
    return std::vector<Node>(0);
  
  }
  map_ = map;


  found_way_.clear();
  open_list_.clear();
  closed_list_.clear();

  //Get real map
  std::vector<Node> real_map = map_->getMap();
  
  //1. Add start node to openl_list
  addNodeOpenList(*start_node_);
  //Needed variable to save the current position
  int current_node_position = 0;
  //2. While the open list is not empty
  double elapsed_time = 0.0;
  double current_time = GetSecondsTime();
  while (!open_list_.empty()){
	  if (show_time) {
		  elapsed_time += GetSecondsTime() - current_time;;
		  current_time = GetSecondsTime();
		 std::cout << "Tiempo: " <<elapsed_time << std::endl;
	  }
    //Get last node, its already ordered by F
    Node current_node = open_list_[open_list_.size() - 1];
    int current_x = current_node.getPosition()[0];
    int current_y = current_node.getPosition()[1];
  

    current_node_position = current_x + (current_y * map_->getWidthMapCollision());
    //If current node equal to final_node
    if (current_node == *target_node_){
      Node way_node = real_map[current_x + (current_y * map_->getWidthMapCollision())];
      //If node is the goal, we create the way and return it!
      while (way_node != *start_node_)
      {

        found_way_.push_back(way_node);
        if (way_node.getParent() != nullptr){
          way_node = *way_node.getParent();
        }
      }
	  return found_way_;

     // return positions;
    }

    //Switch between lists
    open_list_.erase(std::find(open_list_.begin(), open_list_.end(), current_node));
    real_map[current_node_position].closed_ = true;
    //Search for succesors
    for (int x = -1; x <= 1; ++x){
      for (int y = -1; y <= 1; ++y){
        //If is the current node continue whit the next
        if (x == 0 && y == 0)continue;
        int suc_x = (((current_x)+x));
        int suc_y = (((current_y)+y));
        if (suc_x <= 0 || suc_y <= 0 || suc_x >= map_->getWidthMapCollision() || suc_y >= map_->getHeightMapCollision()){
          continue;
        }

        const int position = suc_x +
          (suc_y * map_->getWidthMapCollision());
        if (position > 0 && position < (map_->getWidthMapCollision() * map_->getHeightMapCollision()) ) {

          Node succesor = real_map[position];
          //If not walkable
          if (!succesor.isWalkable())continue;

          //SEARCH AT LIST
         // Node *is_close_list = findNodeCloseList(succesor);

          //continue if  on the closed list
          if (real_map[position].closed_)continue;
          //Search succesor on open list
          Node * is_open_list = findNodeOpenList(succesor);


          if (is_open_list == nullptr){
            //If isnt on the open list, add it!   
            if (x == 0 || y == 0){
              //Verticl or horizantal
              succesor.final_node_ = target_node_;
              succesor.setCosts(current_node.getG() + NORMAL_FhysicalBody);
            }
            else{
              //Diagonals
              succesor.final_node_ = target_node_;
              succesor.setCosts(current_node.getG() + DIAGONAL_FhysicalBody);
            }
            real_map[position].setParent( &real_map[current_node_position] );
            addNodeOpenList(succesor);
          }
          else{
            //If is on the open list
            if (is_open_list->getG() <= succesor.getG()){
              //better way
              if (x == 0 || y == 0){
                //Verticl or horizantal
                succesor.final_node_ = target_node_;
                succesor.setCosts(current_node.getG() + NORMAL_FhysicalBody);
              }
              else{
                //Diagonals
                succesor.final_node_ = target_node_;
                succesor.setCosts(current_node.getG() + DIAGONAL_FhysicalBody);
              }
              real_map[position].setParent( &real_map[current_node_position]);
              addNodeOpenList(succesor);

            }
            else{
              //worst way
              continue;
            }

          } //else if not open list

        } //else not valid position 

      } // Y loop

    }// X loop End nodesuccesors
    
  }

}

void SearchEngine::addNodeOpenList(Node node){
  //ordered by F
  int index = 0;
  while ( ( open_list_.size() > index ) && ( node.getF() < open_list_[index].getF() )){
    ++index;
  }
  open_list_.insert(open_list_.begin() + index, node);
}

Node* SearchEngine::findNodeOpenList(Node node){
  int index = 0;
  while (open_list_.size() > index ){
    if (node == open_list_[index]){
      return &open_list_[index];
    }
    ++index;
  }
  return nullptr;
}

Node* SearchEngine::findNodeCloseList(Node node){
  int index = 0;
  while (closed_list_.size() > index){
    if (node == closed_list_[index]){
      return &closed_list_[index];
    }
    ++index;
  }
  return nullptr;
}