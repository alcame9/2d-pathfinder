#include "../include/map.h"
#include "C2D\sprite.h"
#include "C2D\input.h"
#include "manager.h"
#include "utils.h"
#include "manager.h"
#include "path_finding_engine.h"
#include <iostream>
Map::Map(){
  collision_map_height_ = 0;
  collision_map_width_ = 0;
  not_walkable_color[0] = 255.0f;
  not_walkable_color[1] = 0.0f;
  not_walkable_color[2] = 0.0f;
  not_walkable_color[3] = 255.0f;
}

void Map::init(const char *map_sprite, const char *map_collision_sprite,const char *zones_map){
  readMapSprite(map_sprite);
  readCollisionMapFromImage(map_collision_sprite);
  readZonesMap(zones_map);

}

void Map::readCollisionMapFromImage(const char *file_name){
  C2D::Sprite map_image;
  map_image = C2D::LoadSpriteFromFile(file_name);
  if (file_name == nullptr || map_image == nullptr){
    printf("Image collision not loaded\n");
    return;
  }
  collision_map_height_ = C2D::SpriteHeight(map_image);
  collision_map_width_ = C2D::SpriteWidth(map_image);
  unsigned char color[4];
  for (int y = 0; y < collision_map_height_; y++){
    for (int x = 0; x < collision_map_width_; x++){
		C2D::GetSpritePixel(map_image, x, y, color);
      char c = color[0] + color[1] + color[2];
      if (c == 0){
        //Is not walkable
        Node nod_;
        nod_.init(x, y, nullptr, nullptr, 0,false);
        map_mat_.push_back(nod_);
      }
      else{
        Node nod_;
        nod_.init(x, y, nullptr, nullptr, 0,true);
        map_mat_.push_back(nod_);
      }
    }
  }
  offset_maps_x = map_sprite_w_ / collision_map_width_;
  offset_maps_y = map_sprite_h_ / collision_map_height_;
}

void Map::readMapSprite(const char *file_name){

 map_sprite_ = C2D::LoadSpriteFromFile(file_name);
  if (file_name == nullptr){
    printf("Map sprite not loaded\n");
    return;
  }
  map_sprite_h_ = C2D::SpriteHeight(map_sprite_);
  map_sprite_w_ = C2D::SpriteWidth(map_sprite_);
}

int Map::getHeight(){
  return map_sprite_h_;
}

int Map::getWidth(){
  return map_sprite_w_;
}

int Map::getHeightMapCollision(){
  return collision_map_height_;
}

int Map::getWidthMapCollision(){
  return collision_map_width_;
}

std::vector<Node> Map::getMap(){
  return map_mat_;
}

void Map::render(){
  C2D::DrawSprite(map_sprite_, 0, 0);
  renderGrid();
}



Node* Map::getStart(){
  return &map_mat_[start_x + (start_y * collision_map_width_)];
}
Node* Map::getTarget(){
  return &map_mat_[target_x + (target_y * collision_map_width_)];
}

Node* Map::getRandomNodeFromZone(int zone){
  int random_node = 0;
  switch (zone)
  {
  case WORKING_ZONE:
    random_node = rand() % working_zone_.size();
    if (working_zone_.size() > 0){
      return working_zone_[random_node];
    }
    break;
  case REST_ZONE:
    random_node = rand() % rest_zone_.size();
    return rest_zone_[random_node];
    break;
  case YARD_ZONE:
	  random_node = rand() % yard_zone_.size();
	  return yard_zone_[random_node];
	  break;
  default:
    break;
  }
  return 0;
}

void Map::renderGrid(){
  /*for (int y = 0; y < collision_map_height_; ++y){
    for (int x = 0; x < collision_map_width_; x++){

      if (x == grid_mouse_position_x && y == grid_mouse_position_y){
        float color[4] = { 0.0, 255.0, 0.0, 255.0 };
        renderQuad(x * offset_maps_x, y * offset_maps_y,
          offset_maps_x, offset_maps_y,color );
        continue;
      }
      if (x == start_x && y == start_y){
        float color[4] = { 0.0, 0.0, 255.0, 255.0 };
        renderQuad(x * offset_maps_x, y * offset_maps_y,
          offset_maps_x, offset_maps_y, color);
        continue;
      }
      if (x == target_x && y == target_y){
        float color[4] = { 0.0, 0.0, 0.0, 255.0 };
        renderQuad(x * offset_maps_x, y * offset_maps_y,
          offset_maps_x, offset_maps_y, color);
        continue;
      }
      Node *t_node = &map_mat_[x + (y * collision_map_width_)];
      if (!t_node->isWalkable()){
        renderQuad(x * offset_maps_x, y * offset_maps_y,
                    offset_maps_x,offset_maps_y, not_walkable_color );
      }
      else if (t_node->isWalkable()){
        switch (t_node->zone_type_)
        {
        case WORKING_ZONE:
        {
          float color[4] = { 0.0, 255.0, 255.0, 255.0 };
          renderQuad(x * offset_maps_x, y * offset_maps_y,
            offset_maps_x, offset_maps_y, color);
          continue;
        }
          break;
        case REST_ZONE:
        {
          float color[4] = { 127.0, 64.0, 255.0, 255.0 };
          renderQuad(x * offset_maps_x, y * offset_maps_y,
            offset_maps_x, offset_maps_y, color);
          continue;
        }
          break;
        case YARD_ZONE:
        {
          float color[4] = { 35.0, 64.0, 255.0, 255.0 };
          renderQuad(x * offset_maps_x, y * offset_maps_y,
            offset_maps_x, offset_maps_y, color);
          continue;
        }
        case VALLE_ZONE:
        {
          float color[4] = { 35.0, 64.0, 12.0, 255.0 };
          renderQuad(x * offset_maps_x, y * offset_maps_y,
            offset_maps_x, offset_maps_y, color);
          continue;
        }
        break;
        default:
          break;
        }
      }

    }
  }*/
	if (grid_mouse_position_x != 0.0f && grid_mouse_position_y != 0.0f) {
		float color[4] = { 0.0, 255.0, 0.0, 255.0 };
		renderQuad(grid_mouse_position_x * offset_maps_x, grid_mouse_position_y * offset_maps_y,
			offset_maps_x, offset_maps_y, color);
	}
	if (start_x != 0.0f && start_y != 0.0f) {
		float color_s[4] = { 0.0, 0.0, 255.0, 255.0 };
		renderQuad(start_x * offset_maps_x, start_y * offset_maps_y,
			offset_maps_x, offset_maps_y, color_s);
	}

	if (target_x != 0.0f && target_y != 0.0f) {
		float color[4] = { 0.0, 0.0, 0.0, 255.0 };
		renderQuad(target_x * offset_maps_x, target_y * offset_maps_y,
			offset_maps_x, offset_maps_y, color);
	}
  //render way if exists
  if (!finded_way.empty()){
    renderWay(finded_way);
  }
}

void Map::renderQuad(int x, int y, int widht, int heigh, float color_data[4]) {
  float r = color_data[0];
  float g = color_data[1];
  float b = color_data[2];
  float a = color_data[3];
  float p[] = {
    x, y,
    x + widht, y,
    x + widht, y + heigh,
    x, y + heigh,
    x, y // last point connects with the first one
  };
  C2D::SetColorFillShape(r, g, b);
  
  C2D::DrawSolidShape(5, p,true);
}

void Map::inputMap(){
  
  float pos_x = C2D::Input::GetMouseX();
  float pos_y = C2D::Input::GetMouseY();
  int c_x = pos_x / (map_sprite_w_ / collision_map_width_);
  int c_y = pos_y / (map_sprite_h_ / collision_map_height_);


  int p = c_x + (c_y * collision_map_width_);
  if (p < map_mat_.size()){
    grid_mouse_position_x = c_x;
    grid_mouse_position_y= c_y;
  }

  if (C2D::Input::GetButtonMousePressed(C2D::Input::kMouseButtonsLeft)) {
    start_x = grid_mouse_position_x;
    start_y = grid_mouse_position_y;
  }
  else if (C2D::Input::GetButtonMousePressed(C2D::Input::kMouseButtonRight)){
    target_x = grid_mouse_position_x;
    target_y = grid_mouse_position_y;
  }
  
  if (C2D::Input::IsSpecialKeyPressed(C2D::Input::kSpecialKeysSpace)) {
	 finded_way = Manager::instance().GetPathfinder()->findWay(this, getStart(), getTarget(),true);
  }
} 

void Map::setFindedWay(std::vector<Node> way){
  finded_way = way;
}
void Map::renderWay(std::vector<Node> way){
  float color[4] = { 0.0, 255.0, 0.0, 255.0 };
  for (int i = 0; i < way.size(); i++){
    Node T_node = way[i];
    
    renderQuad(T_node.getPosition()[0] * offset_maps_x, T_node.getPosition()[1] * offset_maps_y,
      offset_maps_x, offset_maps_y, color);
  }
}


void Map::readZonesMap(const char* file_name){
C2D::Sprite map = C2D::LoadSpriteFromFile(file_name);
  int map_w = C2D::SpriteWidth(map);
  int map_h = C2D::SpriteHeight(map);
  unsigned char color[4];
  std::vector<Node *> working_area;
  std::vector<Node *> rest_area;
  for (int y = 0; y < map_h - 1; y++){
    for (int x = 0; x < map_w - 1; x++){
	C2D::GetSpritePixel(map, x, y, color);
      int total_color = color[0] + color[1] + color[2];     
      int new_x = x / offset_maps_x;
      int new_y = y / offset_maps_y;
      if ( total_color == WORK_COLOR ){
        //Working area
        map_mat_[new_x + (new_y * collision_map_width_)].zone_type_ = WORKING_ZONE;
        working_area.push_back(&map_mat_[new_x + (new_y *collision_map_width_)]);
      }
      else if (total_color == REST_COLOR){
        //Rest area
        map_mat_[new_x + (new_y*collision_map_width_)].zone_type_ = REST_ZONE;
        rest_area.push_back(&map_mat_[new_x + (new_y*collision_map_width_)]);
      }
      else if (total_color == YARD_COLOR){
        map_mat_[new_x + (new_y*collision_map_width_)].zone_type_ = YARD_ZONE;
        yard_zone_.push_back(&map_mat_[new_x + (new_y*collision_map_width_)]);
      }
      else if (total_color == VALLE_COLOR){
        map_mat_[new_x + (new_y*collision_map_width_)].zone_type_ = VALLE_ZONE;
     //   yard_zone_.push_back(&map_mat_[new_x + (new_y*collision_map_width_)]);
      }
      else if (color[0] == 255.0){
       // map_mat_[new_x + (new_y*collision_map_width_)].is_door_ = true;
      }
      else if (total_color == HALL_COLOR){
        map_mat_[new_x + (new_y*collision_map_width_)].zone_type_ = HALL_ZONE;
       
      }
    }
  }
  working_zone_ = working_area;
  rest_zone_ = rest_area;
	

  
}

Node* Map::getNodeByScreenPosition(int x, int y){
  
  return &map_mat_[(x / map_sprite_w_) + ((y / map_sprite_h_) *collision_map_width_)];
}

