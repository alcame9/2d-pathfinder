#include "node.h"
#include <math.h>
Node::Node(){}

Node::Node(const Node & node){
	x_ = node.x_;
	y_ = node.y_;
	parent_node_ = node.parent_node_;
	final_node_ = node.final_node_;
	g_ = node.g_;
	walkable_ = node.walkable_;
	f_ = node.f_;
	closed_ = node.closed_;
}

int * Node::getPosition(){
  int out[2] = { x_, y_ };
  return out;
}

bool Node::operator==(const Node& rh){
  if (rh.x_ == this->x_ && rh.y_ == this->y_)return true;
  return false;
}

bool Node::operator!=(const Node& rh){
  if (rh.x_ != this->x_ || rh.y_ != this->y_)return true;
  return false;
}

void Node::init(int x, int y, 
                Node *parent_node, 
                Node* final_node,
               int total_g,
               bool walkable){
  x_ = x;
  y_ = y;
  parent_node_ = parent_node;
  final_node_ = final_node;
  g_ = total_g;
  walkable_ = walkable;
  if (final_node != nullptr){
    f_ = total_g + computeH();
  }
  closed_ = false;
}

Node * Node::getParent(){
  return parent_node_;
}

bool Node::isWalkable(){
  return walkable_;
}

void Node::setWalkable(bool walkable){
  walkable_ = walkable;
}

int Node::getF(){
  return f_;
}
// :) by Javi
int Node::getG(){
  return g_;
}
//Private

int Node::computeH(){
  int x = x_ - final_node_->getPosition()[0];
  int y = y_ - final_node_->getPosition()[1];

  if (x < 0) x *= -1;
  if (y < 0) y *= -1;

  h_ = 10 * (x + y); //Manhatan
  return h_;
}

void Node::setCosts(int cost_g){
  g_ = cost_g;
  f_ = g_ + computeH();
}

void Node::setParent(Node *parent){
  parent_node_ = parent;
}