-- Create Solution

solution "Pathfinder"

configurations{"Debug"}
location("debug")
targetdir("debug/bin")
debugdir("data")



project "Pathfinder"
	kind "ConsoleApp"
	language "C++"
	defines {}
	links{"opengl32","C2DBuild"}
	libdirs{"lib"}
	includedirs{"include","deps"}
	files{"include/**.h","src/**.cc","src/**.c","src/**.cpp"}

